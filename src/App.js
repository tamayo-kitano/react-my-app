import React from 'react';
import {Toolbar} from '@material-ui/core';
import {BrowserRouter,Route,Link,Switch} from "react-router-dom";

import LoginForm from "./login-form";
import ItemList from './item-list';
import Home from  './home';

const App = (props) => {
    //BrowserRouterコンポーネントによりルーティングを行う場として設定される
  return (
      <BrowserRouter>
          <div>
              <Toolbar>
                  {/*aタグを使う代わりにLinkタグ*/}
                  <Link to = '/'>トップ</Link>-
                  <Link to = '/login'>ログイン</Link>-
                  <Link to = '/list'>一覧</Link>
                  {/*to属性でこのリンクをクリックした際の遷移先URLを指定*/}
              </Toolbar>
              {/*Switchコンポーネントで指定されたURLに合致するコンポーネントにページが切り替わるようにする*/}
              <Switch>
                  {/*Routeコンポーネントでルーティングの設定を行う*/}
                  <Route exact path ='/' component={Home}/>
                  <Route  path ='/login' component={LoginForm}/>
                  <Route  path ='/list' component={ItemList}/>
              </Switch>
          </div>
      </BrowserRouter>

  );
};
export default App;
