import React from 'react';
import {Paper,TextField,Typography,Button}from'@material-ui/core' ;
import {makeStyles} from '@material-ui/core/styles';

//styleオブジェクトを返すフックを作成
const useStyles= makeStyles({
    container:{
        display:'flex',height:'100vh',backgroundColor:'#eeeeee'
    },
    loginBase:{
        width:'300px',height:'200px',margin:'auto',padding:'10px'
    },
    field:{width:'80%'},
    loginButton:{margin:'15px'}
});

const LoginForm = () => {
    //styleオブジェクトをフック的に利用
    const classes = useStyles();
    //className属性を使って各コンポーネントにスタイルを適用
    return (
        <div className={classes.container}>
            <Paper className={classes.loginBase}>
                <Typography>ログインしてください。</Typography>
                <TextField className={classes.field} label='Name' />
                <TextField className={classes.field} label='Password' />
                <Button className={classes.loginButton}
                 variant='contained' color='primary'>
            ログイン
        </Button>
     </Paper>
  </div>
    );
};

export default LoginForm;